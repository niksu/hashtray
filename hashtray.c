/*
Copyright University of Pennsylvania, 2018

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


This file: Partial-key cuckoo hash.
(aka A cuckoo filter with a value associated with each fingerprint/key)

NOTE: this code is thread-safe in "regular mode", but the debug
      mode (-DREMEMBER_LOSS -DREMEMBER_COLLISIONS) hasn't been tuned for
      thread-safety.
*/


#ifdef HASHTRAY_ASSERT
#include <assert.h>
#endif // HASHTRAY_ASSERT

#ifdef REMEMBER_COLLISIONS
#include <assert.h>
#include "stdio.h"
#endif // REMEMBER_COLLISIONS

#ifdef HASHTRAY_LOG_INSERTS
#include <stdio.h>
#endif // HASHTRAY_LOG_INSERTS

#ifdef REMEMBER_LOSS
#include <assert.h>
#include "stdio.h"
#endif // REMEMBER_LOSS

#if defined(REMEMBER_LOSS) || defined(REMEMBER_COLLISIONS)
#include "hashtray_debug.h"
#endif // defined(REMEMBER_LOSS) || defined(REMEMBER_COLLISIONS)

#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#if defined(MULTITHREADED) && defined(MULTIPROCESS)
#error Simultaneous MULTITHREADED and MULTIPROCESS not supported.
#endif // defined(MULTITHREADED) && defined(MULTIPROCESS)

#if defined(MULTITHREADED) || defined(MULTIPROCESS)
#include <time.h>
#endif // defined(MULTITHREADED) || defined(MULTIPROCESS)

#ifdef MULTITHREADED
#include <pthread.h>
#endif // MULTITHREADED

#ifdef MULTIPROCESS
#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <sys/types.h>
#endif // MULTIPROCESS

#include "hashtray_hash.h"

struct idxs {
  HASHTRAY(key_t) idx[CHOICES];
};

static HASHTRAY(key_t) alt_idx(HASHTRAY(key_t) idx, HASHTRAY(key_t) fingerprint);
static struct idxs idxs_of_DATA_TYPE(HASHTRAY(data_t) data, HASHTRAY(key_t) * fingerprint);

struct entry {
  bool clear;
  HASHTRAY(key_t) key;
  HASHTRAY(value_t) value;
};

struct cell {
  struct entry entry[NUM_CELL_ENTRIES];
};

struct HASHTRAY(table) {
  struct cell cell[TABLE_SIZE];
#ifdef MULTITHREADED
  pthread_mutex_t lock[TABLE_SIZE];
#endif // MULTITHREADED
#ifdef MULTIPROCESS
  sem_t * lock[TABLE_SIZE];
#endif // MULTIPROCESS
};

#ifdef REMEMBER_LOSS
struct overfill_t {
  struct entry entry[NUM_OVERFILL_ENTRIES];
} overfill;

int overfill_idx = 0;

void
print_overfill(bool show_entries)
{
  assert(overfill_idx >= 0);
  printf("overfill_idx=%d\n", overfill_idx);
  if (show_entries) {
    for (int idx = 0; idx < overfill_idx; idx++) {
      printf("%d. key=%d, value=%d\n", idx, overfill.entry[idx].key,
          overfill.entry[idx].value);
    }
  }
}

void
reset_overfill(void)
{
  overfill_idx = 0;
}

bool
has_overflowed(HASHTRAY(data_t) data) {
  // Check if the item is in the "overflow" array.
  bool item_found = false;
  for (int idx = 0; idx < overfill_idx; idx++) {
    if (data == overfill.entry[idx].key) {
      item_found = true;
      break;
    }
  }
  return item_found;
}
#endif // REMEMBER_LOSS

#ifdef REMEMBER_COLLISIONS
struct collision_t {
  struct entry entry[NUM_COLLIDED_ENTRIES];
  struct entry collided_with[NUM_COLLIDED_ENTRIES];
} collision;
int collision_idx = 0;

void
print_collision(bool show_entries)
{
  assert(collision_idx >= 0);
  printf("collision_idx=%d\n", collision_idx);
  if (show_entries) {
    for (int idx = 0; idx < collision_idx; idx++) {
      printf("%d. key=%d, value=%d (collided with key=%d, value=%d)\n",
          idx, collision.entry[idx].key, collision.entry[idx].value,
          collision.collided_with[idx].key, collision.collided_with[idx].value);
    }
  }
}

void
reset_collision(void)
{
  collision_idx = 0;
}

bool
has_collided(HASHTRAY(data_t) data, HASHTRAY(value_t) queried_metadata) {
  assert(collision_idx > 0);
  HASHTRAY(key_t) fingerprint = HASHTRAY(fingerprint_of_DATA_TYPE)(data);
  bool found_collision = false;
  for (int idx = 0; idx < collision_idx; idx++) {
    if (fingerprint == collision.entry[idx].key ||
        fingerprint == collision.collided_with[idx].key) {
      if (queried_metadata == collision.entry[idx].value ||
          queried_metadata == collision.collided_with[idx].value) {
        found_collision = true;
        break;
      }
    }
  }
  return found_collision;
}
#endif // REMEMBER_COLLISIONS

const char * HASHTRAY(outcome_str)[] =
  {"OK", "NOT_FOUND", "GAVE_UP", "BLOCKS_FULL"};

static HASHTRAY(key_t)
alt_idx(HASHTRAY(key_t) idx, HASHTRAY(key_t) fingerprint)
{
  // FIXME assuming CHOICE==2
  HASHTRAY(key_t) h1 = HASHTRAY(hash_of_KEY_TYPE)(0, fingerprint);
  HASHTRAY(key_t) h2 = HASHTRAY(hash_of_KEY_TYPE)(1, fingerprint);
  if (idx == h1) {
    return h2;
  } else {
#ifdef HASHTRAY_ASSERT
    assert(idx == h2);
#endif // HASHTRAY_ASSERT
    return h1;
  }
}

struct idxs
idxs_of_DATA_TYPE(HASHTRAY(data_t) data, HASHTRAY(key_t) * fingerprint)
{
  struct idxs result;
  *fingerprint = HASHTRAY(fingerprint_of_DATA_TYPE)(data);
  for (int i = 0; i < CHOICES; i++) {
    result.idx[i] = HASHTRAY(hash_of_KEY_TYPE)(i, *fingerprint);
  }
  // FIXME how do we ensure that, for all i and j, result.idx[i] != result.idx[j]
#ifdef HASHTRAY_ASSERT
  for (int i = 0; i < CHOICES; i++) {
    assert((int)result.idx[i] >= 0);
    assert((int)result.idx[i] < TABLE_SIZE);
  }
#endif // HASHTRAY_ASSERT
  return result;
}

static inline void
unlock_index(struct HASHTRAY(table) * t, int table_idx) {
  int error;
#if !defined(MULTITHREADED) && !defined(MULTIPROCESS)
  // Do nothing

#elif defined(MULTITHREADED) && defined(MULTIPROCESS)
#error Simultaneous MULTITHREADED and MULTIPROCESS not supported.

#elif defined(MULTITHREADED)
  error = pthread_mutex_unlock(&(t->lock[table_idx]));
#ifdef HASHTRAY_ASSERT
  assert(!error); // FIXME check when !HASHTRAY_ASSERT
#endif // HASHTRAY_ASSERT

#elif defined(MULTIPROCESS)
  error = sem_post(t->lock[table_idx]);
#ifdef HASHTRAY_ASSERT
  assert(!error); // FIXME check when !HASHTRAY_ASSERT
#endif // HASHTRAY_ASSERT

#endif
}

static inline void
lock_index(struct HASHTRAY(table) * t, int table_idx) {
  int error;
#if !defined(MULTITHREADED) && !defined(MULTIPROCESS)
  // Do nothing

#elif defined(MULTITHREADED) && defined(MULTIPROCESS)
#error Simultaneous MULTITHREADED and MULTIPROCESS not supported.

#elif defined(MULTITHREADED)
  error = pthread_mutex_lock(&(t->lock[table_idx]));
#ifdef HASHTRAY_ASSERT
  assert(!error); // FIXME check when !HASHTRAY_ASSERT
#endif // HASHTRAY_ASSERT

#elif defined(MULTIPROCESS)
  error = sem_wait(t->lock[table_idx]);
#ifdef HASHTRAY_ASSERT
  assert(!error); // FIXME check when !HASHTRAY_ASSERT
#endif // HASHTRAY_ASSERT

#endif
}

static inline void
lock_indices(struct HASHTRAY(table) * t, struct idxs is) {
#if !defined(MULTITHREADED) && !defined(MULTIPROCESS)
  // Do nothing

#elif defined(MULTITHREADED) && defined(MULTIPROCESS)
#error Simultaneous MULTITHREADED and MULTIPROCESS not supported.

#else
  // We lock both choices at a go, but owing to the sequential nature of
  // locking we could end up with deadlock, so we give up after a short timeout
  // and retry, to give the other side the chance to lock.
  while (true) { // FIXME risk of infinite execution
    int idx = 0;
    for (; idx < CHOICES; idx++) {
      int result;

#ifdef MULTITHREADED
      result = pthread_mutex_trylock(&(t->lock[(int)is.idx[idx]])); // FIXME strictly speaking should check for EBUSY == result
#endif // MULTITHREADED

#ifdef MULTIPROCESS
      result = sem_trywait(t->lock[(int)is.idx[idx]]);
#endif // MULTIPROCESS

      if (0 != result) {
        // Unlock everything, wait a tiny amount of time and try again.
        for (int idy = 0; idy < idx; idy++) {
          unlock_index(t, (int)is.idx[idy]);
        }
        struct timespec req = {.tv_sec = 0,
          .tv_nsec = (1000 * (rand() % BACKOFF_SLEEP_MICROSEC))};
        struct timespec rem;
        nanosleep(&req, &rem); // FIXME ignoring return value
        break;
      }
    }

    if (CHOICES == idx) {
      break;
    }
  }
#endif
}

static inline void
unlock_indices_except(struct HASHTRAY(table) * t, struct idxs is, int * opt_dont_unlock) {
#if !defined(MULTITHREADED) && !defined(MULTIPROCESS)
  // Do nothing

#elif defined(MULTITHREADED) && defined(MULTIPROCESS)
#error Simultaneous MULTITHREADED and MULTIPROCESS not supported.

#else
  for (int idx = 0; idx < CHOICES; idx++) {
    int table_idx = (int)is.idx[idx];
    if (NULL == opt_dont_unlock ||
        (NULL != opt_dont_unlock &&
         *opt_dont_unlock != table_idx)) {
      unlock_index(t, table_idx);
    }
  }
#endif
}

enum HASHTRAY(outcome)
HASHTRAY(insert)(struct HASHTRAY(table) * t, HASHTRAY(data_t) data, HASHTRAY(data_t) metadata,
   int (*merge_fun)(HASHTRAY(data_t) * stored, const HASHTRAY(data_t) * new),
   int (*expiry_fun)(const HASHTRAY(data_t) * metadata))
{
  HASHTRAY(key_t) fingerprint;
  struct idxs is = idxs_of_DATA_TYPE(data, &fingerprint);
#ifdef HASHTRAY_LOG_INSERTS
  printf("data=%u metadata=%d fingerprint=%d is.idx[0]=%d is.idx[1]=%d\n",
      data, metadata, fingerprint, is.idx[0], is.idx[1]);
#endif // HASHTRAY_LOG_INSERTS

  lock_indices(t, is);

#ifdef REMEMBER_COLLISIONS
  for (int idx = 0; idx < CHOICES; idx++) {
    int table_idx = (int)is.idx[idx];
    for (int i = 0; i < NUM_CELL_ENTRIES; i++) {
      if (!t->cell[table_idx].entry[i].clear &&
          t->cell[table_idx].entry[i].key == fingerprint) {
        // We judge that a collision has occurred.
#ifdef HASHTRAY_ASSERT
        assert(collision_idx < NUM_COLLIDED_ENTRIES);
#endif // HASHTRAY_ASSERT
        collision.entry[collision_idx].key = fingerprint;
        collision.entry[collision_idx].value = metadata;
        collision.collided_with[collision_idx].key = t->cell[table_idx].entry[i].key;
        collision.collided_with[collision_idx].value = t->cell[table_idx].entry[i].value;
#ifdef HASHTRAY_DESCRIBE_COLLISIONS
        printf("(%d, %d) collided with (%d, %d) on table_idx=%d, entry=%d\n",
        collision.entry[collision_idx].key,
        collision.entry[collision_idx].value,
        collision.collided_with[collision_idx].key,
        collision.collided_with[collision_idx].value,
        table_idx, i);
#endif // HASHTRAY_DESCRIBE_COLLISIONS
        collision_idx += 1;
      }
    }
  }
#endif // REMEMBER_COLLISIONS

  // This is the bit that actually does the inserting.
  // We check both alternatives before updating the table otherwise we could
  // end up with multiple values for the same key.
  bool exists = false;
  int table_idx;
  int entry_idx;
  // Keep track of free entries we can insert into.
  bool found_free_entry = false;
  int free_table_idx;
  int free_entry_idx;
  for (int idx = 0; idx < CHOICES; idx++) {
    table_idx = (int)is.idx[idx];
    for (entry_idx = 0; entry_idx < NUM_CELL_ENTRIES; entry_idx++) {
      if (t->cell[table_idx].entry[entry_idx].clear &&
          !found_free_entry/*Only need one free entry*/) {
        found_free_entry = true;
        free_table_idx = table_idx;
        free_entry_idx = entry_idx;
      }

      if (!t->cell[table_idx].entry[entry_idx].clear &&
        (t->cell[table_idx].entry[entry_idx].key == fingerprint)) {
        exists = true;
        break;
      }
    }

    if (exists) {
      break;
    }
  }

  int delete = 0;
  if (exists) {
#ifdef HASHTRAY_ASSERT
    assert(!t->cell[table_idx].entry[entry_idx].clear);
    assert(t->cell[table_idx].entry[entry_idx].key == fingerprint);
#endif // HASHTRAY_ASSERT
    if (NULL == merge_fun) {
      t->cell[table_idx].entry[entry_idx].value = metadata;
    } else {
      delete = merge_fun(&(t->cell[table_idx].entry[entry_idx].value), &metadata);
      if (0 != delete) {
        t->cell[free_table_idx].entry[free_entry_idx].clear = true;
      }
    }
  } else if (found_free_entry) {
    t->cell[free_table_idx].entry[free_entry_idx].clear = false;
    t->cell[free_table_idx].entry[free_entry_idx].key = fingerprint;
    t->cell[free_table_idx].entry[free_entry_idx].value = metadata;
  }

  if (exists || found_free_entry) {
    // We can unlock everything and return.
    unlock_indices_except(t, is, NULL);
    if (0 != delete) {
      return HASHTRAY(NOT_FOUND);
    } else {
      return HASHTRAY(OK);
    }
  }

  // At this point we haven't been able to make the insertion, since both cells
  // were already full.
#ifdef HASHTRAY_FAIL_EAGERLY
  // Unlock everything and give up.
  unlock_indices_except(t, is, NULL);

  return HASHTRAY(BLOCKS_FULL);
#else // ndef HASHTRAY_FAIL_EAGERLY

  table_idx = (int)is.idx[(int)rand() % CHOICES];

  // We're going to have to kick stuff out. We unlock all except the cell we
  // choose _not_ to kick stuff out of and continue.
  unlock_indices_except(t, is, &table_idx);

  HASHTRAY(key_t) swapped_key;
  HASHTRAY(value_t) swapped_value;
  for (int try_num = 0; try_num < MAX_KICKOUTS; try_num++) {
    int entry = (int)rand() % NUM_CELL_ENTRIES;
    // FIXME could iterate through entries to find a free one, rather do
    //       unnecessary kicking by picking a random value for "entry".

    // FIXME check for collision among the other entries (which might
    //       have been moved to an alternative bucket). But beware of
    //       over-reporting collisions, e.g., if 2 entries get kicked
    //       down a similar path, it might be counted as multiple collisions
    //       rather than a single one.

    if (NULL != expiry_fun &&
        expiry_fun(&(t->cell[table_idx].entry[entry].value)) == 0) {
      // This item isn't expired. Pick another item.
      // FIXME relying on rand() to find items might not be a good idea.
      continue;
    }

    swapped_key = t->cell[table_idx].entry[entry].key;
    swapped_value = t->cell[table_idx].entry[entry].value;
    // t->cell[table_idx] is already locked at this point, so we can update it
    // safely.
    t->cell[table_idx].entry[entry].key = fingerprint;
    t->cell[table_idx].entry[entry].value = metadata;

    // This won't be true the first time we go through this loop, since we
    // wouldn't have entered the "kick out" phase if there were empty cells.
    if (t->cell[table_idx].entry[entry].clear) {
      t->cell[table_idx].entry[entry].clear = false;
      unlock_index(t, table_idx);
      // We have filled an empty entry (i.e., it's "clear" flag was set) so
      // there's no need to do further kicking.
      return HASHTRAY(OK);
    }

    fingerprint = swapped_key;
    metadata = swapped_value;
    unlock_index(t, table_idx);
    // NOTE in addition to exploring the alternative block we could also explore
    //      a fingerprint's "non-alternative" block for available entries --
    //      that is, pick some other fingerprint in the current block and
    //      attempt to kick it, rather than the current fingerprint; but it's
    //      not obvious which to pick, so the current approach feels simplest.
    table_idx = (int)alt_idx((HASHTRAY(key_t))table_idx, fingerprint);
    lock_index(t, table_idx);
  }

  // If we reached this point then we exceeded MAX_KICKOUTS. We're giving up
  // with the propagation.
#ifdef REMEMBER_LOSS
  assert(overfill_idx < NUM_OVERFILL_ENTRIES);
  // Record which items got kicked out of the table.
  // NOTE This behaviour might be exploited, to have elements of the table
  //      erased (having them kicked out), if an adversary can engineer a
  //      series of moves.
  overfill.entry[overfill_idx].clear = false;
  overfill.entry[overfill_idx].key = fingerprint;
  overfill.entry[overfill_idx].value = metadata;
  overfill_idx += 1;
#endif // REMEMBER_LOSS
  return HASHTRAY(GAVE_UP);
#endif // HASHTRAY_FAIL_EAGERLY
}

enum HASHTRAY(outcome)
HASHTRAY(delete)(struct HASHTRAY(table) * t, HASHTRAY(data_t) data)
{
  enum HASHTRAY(outcome) result = HASHTRAY(NOT_FOUND);
  HASHTRAY(key_t) fingerprint;
  struct idxs is = idxs_of_DATA_TYPE(data, &fingerprint);
  lock_indices(t, is);

  for (int idx = 0; idx < CHOICES; idx++) {
    int table_idx = (int)is.idx[idx];
    for (int i = 0; i < NUM_CELL_ENTRIES; i++) {
      if (! t->cell[table_idx].entry[i].clear &&
          t->cell[table_idx].entry[i].key == fingerprint) {
        t->cell[table_idx].entry[i].clear = true;
        result = HASHTRAY(OK);
        break;
      }
    }

    if (HASHTRAY(OK) == result) {
      break;
    }
  }

  unlock_indices_except(t, is, NULL);
  return result;
}

enum HASHTRAY(outcome)
HASHTRAY(lookup)(struct HASHTRAY(table) * t, HASHTRAY(data_t) data, HASHTRAY(data_t) * metadata,
    int (*apply_fun)(HASHTRAY(data_t) * metadata))
{
  bool done = false;
  enum HASHTRAY(outcome) result = HASHTRAY(NOT_FOUND);
  HASHTRAY(key_t) fingerprint;
  struct idxs is = idxs_of_DATA_TYPE(data, &fingerprint);
  lock_indices(t, is);

  for (int idx = 0; idx < CHOICES; idx++) {
    int table_idx = (int)is.idx[idx];
    for (int i = 0; i < NUM_CELL_ENTRIES; i++) {
      if (! t->cell[table_idx].entry[i].clear &&
          t->cell[table_idx].entry[i].key == fingerprint) {

        int delete = 0;
        if (NULL != apply_fun) {
          delete = apply_fun(&(t->cell[table_idx].entry[i].value));
        }

        if (0 != delete) {
          t->cell[table_idx].entry[i].clear = 1;
          result = HASHTRAY(NOT_FOUND);
        } else {
          *metadata = t->cell[table_idx].entry[i].value;
          result = HASHTRAY(OK);
        }
        done = true;
        break;
      }
    }

    if (done) {
      break;
    }
  }

  unlock_indices_except(t, is, NULL);
  return result;
}

#ifdef MULTIPROCESS
static void
semaphore_name(char * buf, int i) {
  // FIXME not checking buf size
  sprintf(buf, "/HASHTRAY_sem_%d"/*FIXME const -- make this into parameter*/,
      i);
}
#endif // MULTIPROCESS

struct HASHTRAY(table) *
HASHTRAY(create_table)(void)
{
#ifdef MULTIPROCESS
  struct HASHTRAY(table) * t = mmap(NULL, sizeof(*t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
#ifdef HASHTRAY_ASSERT
    assert(MAP_FAILED != t); // FIXME check when !HASHTRAY_ASSERT
#endif // HASHTRAY_ASSERT
#else
  struct HASHTRAY(table) * t = malloc(sizeof(*t));
#endif // MULTIPROCESS
  for (int table_idx = 0; table_idx < TABLE_SIZE; table_idx++) {
    for (int i = 0; i < NUM_CELL_ENTRIES; i++) {
      t->cell[table_idx].entry[i].clear = true;
    }
#ifdef MULTITHREADED
    int error = pthread_mutex_init(&(t->lock[table_idx]), NULL);
#ifdef HASHTRAY_ASSERT
    assert(!error); // FIXME check when !HASHTRAY_ASSERT
#endif // HASHTRAY_ASSERT
#endif // MULTITHREADED

#ifdef MULTIPROCESS
    // Not using sem_init() since it's not supported on MacOS;
    // so instead I'm making do with sem_open().
#if 0
    sem_init(&(t->lock[table_idx]), 1, 1);
#else
    char name[20];
    semaphore_name(name, table_idx);
    t->lock[table_idx] = sem_open(name, O_CREAT /* FIXME | O_EXCL*/, 0600, 1);
#ifdef HASHTRAY_ASSERT
    assert(SEM_FAILED != t->lock[table_idx]); // FIXME check when !HASHTRAY_ASSERT
#endif // HASHTRAY_ASSERT
#endif
#endif // MULTIPROCESS
  }
  return t;
}

void
HASHTRAY(destroy_table)(struct HASHTRAY(table) * t)
{
  int error;
  for (int table_idx = 0; table_idx < TABLE_SIZE; table_idx++) {
#ifdef MULTITHREADED
    error = pthread_mutex_destroy(&(t->lock[table_idx]));
#ifdef HASHTRAY_ASSERT
    assert(!error); // FIXME check when !HASHTRAY_ASSERT
#endif // HASHTRAY_ASSERT
#endif // MULTITHREADED

#ifdef MULTIPROCESS
    error = sem_close(t->lock[table_idx]);
#ifdef HASHTRAY_ASSERT
    assert(0 == error); // FIXME check when !HASHTRAY_ASSERT
#endif // HASHTRAY_ASSERT

    char name[20];
    semaphore_name(name, table_idx);
    error = sem_unlink(name);
#ifdef HASHTRAY_ASSERT
    assert(0 == error); // FIXME check when !HASHTRAY_ASSERT
#endif // HASHTRAY_ASSERT

#endif // MULTIPROCESS
  }
#ifdef MULTIPROCESS
  error = munmap(t, sizeof(*t));
#ifdef HASHTRAY_ASSERT
  assert(0 == error); // FIXME check when !HASHTRAY_ASSERT
#endif // HASHTRAY_ASSERT
#else
  free(t);
#endif // MULTIPROCESS
}

int
HASHTRAY(serialise_table)(struct HASHTRAY(table) * t, char ** buffer)
{
  const int buffer_size = TABLE_SIZE * NUM_CELL_ENTRIES * sizeof(struct entry);
  *buffer = malloc(buffer_size);
  if (NULL == *buffer) {
    return -1;
  }

  int idx = 0;

  for (int table_idx = 0; table_idx < TABLE_SIZE; table_idx++) {
    lock_index(t, table_idx);
    for (int entry_idx = 0; entry_idx < NUM_CELL_ENTRIES; entry_idx++) {
      memcpy(*buffer + idx, &(t->cell[table_idx].entry[entry_idx]), sizeof(struct entry));
      idx += sizeof(struct entry);
    }
    unlock_index(t, table_idx);
  }

  return buffer_size;
}

int
HASHTRAY(deserialise_table)(const char * buffer, const int buffer_len, struct HASHTRAY(table) * t)
{
  if (NULL == buffer) {
    return -1;
  }
  const int buffer_size = TABLE_SIZE * NUM_CELL_ENTRIES * sizeof(struct entry);
  if (buffer_size != buffer_len) {
    return -1;
  }

  int idx = 0;

  for (int table_idx = 0; table_idx < TABLE_SIZE; table_idx++) {
    lock_index(t, table_idx);
    for (int entry_idx = 0; entry_idx < NUM_CELL_ENTRIES; entry_idx++) {
      memcpy(&(t->cell[table_idx].entry[entry_idx]), buffer + idx, sizeof(struct entry));
      idx += sizeof(struct entry);
    }
    unlock_index(t, table_idx);
  }

  return 0;
}

void
HASHTRAY(keys_of_table)(struct HASHTRAY(table) * t, HASHTRAY(key_t) ** result_array, int * result_array_len)
{
  const int buffer_size = TABLE_SIZE * NUM_CELL_ENTRIES * sizeof(HASHTRAY(key_t));
  HASHTRAY(key_t) * buffer = malloc(buffer_size);
  if (NULL == buffer) {
    *result_array = NULL;
    *result_array_len = -1;
    return;
  }

  unsigned idx = 0;

  for (int table_idx = 0; table_idx < TABLE_SIZE; table_idx++) {
    lock_index(t, table_idx);
    for (int entry_idx = 0; entry_idx < NUM_CELL_ENTRIES; entry_idx++) {
      if (! t->cell[table_idx].entry[entry_idx].clear) {
        buffer[idx] = t->cell[table_idx].entry[entry_idx].key;
        idx += 1;
      }
    }
    unlock_index(t, table_idx);
  }

  if (0 == idx) {
    *result_array = NULL;
  } else {
    *result_array = malloc(idx * sizeof(HASHTRAY(key_t)));
    if (NULL == *result_array) {
      *result_array_len = -1;
      return;
    }
    for (*result_array_len = 0; *result_array_len <= (int)idx; (*result_array_len)++) {
      (*result_array)[*result_array_len] = buffer[*result_array_len];
    }
  }

  free(buffer);
  *result_array_len = (int)idx;
  return;
}

// NOTE DRY principle: this code is very similar to that of HASHTRAY(keys_of_table)() (except that it uses values instead of keys).
void
HASHTRAY(values_of_table)(struct HASHTRAY(table) * t, HASHTRAY(value_t) ** result_array, int * result_array_len)
{
  const int buffer_size = TABLE_SIZE * NUM_CELL_ENTRIES * sizeof(HASHTRAY(value_t));
  HASHTRAY(value_t) * buffer = malloc(buffer_size);
  if (NULL == buffer) {
    *result_array = NULL;
    *result_array_len = -1;
    return;
  }

  unsigned idx = 0;

  for (int table_idx = 0; table_idx < TABLE_SIZE; table_idx++) {
    lock_index(t, table_idx);
    for (int entry_idx = 0; entry_idx < NUM_CELL_ENTRIES; entry_idx++) {
      if (! t->cell[table_idx].entry[entry_idx].clear) {
        buffer[idx] = t->cell[table_idx].entry[entry_idx].value;
        idx += 1;
      }
    }
    unlock_index(t, table_idx);
  }

  if (0 == idx) {
    *result_array = NULL;
  } else {
    *result_array = malloc(idx * sizeof(HASHTRAY(value_t)));
    if (NULL == *result_array) {
      *result_array_len = -1;
      return;
    }
    for (*result_array_len = 0; *result_array_len <= (int)idx; (*result_array_len)++) {
      (*result_array)[*result_array_len] = buffer[*result_array_len];
    }
  }

  free(buffer);
  *result_array_len = (int)idx;
  return;
}

int
HASHTRAY(rand_range)(int min, int max)
{
#ifdef HASHTRAY_ASSERT
  assert(min >= 0);
  assert(max >= min);
#endif // HASHTRAY_ASSERT

  if (min == max) {
    return min;
  }

  return min + (rand() % (max - min));
}
